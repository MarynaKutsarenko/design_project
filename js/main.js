(function () {
   const burger = document.querySelector('.nav_burger');
   const menuList = document.querySelector('.nav_item');

   burger.addEventListener('click', () => {
      if (!burger.classList.contains('burger_active')) {
         console.log('hi');
         menuList.style.display = 'block';
      } else {
         menuList.style.display = 'none';
      }

      burger.classList.toggle('burger_active');
   });
}());

